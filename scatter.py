import helper
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def compute_daily_returns(df):
    daily_returns = df.copy()
    daily_returns[1:] = (df[1:] / df[:-1].values) -1 #need to use .values otherwise the element wise arithmatic will not work
    daily_returns.ix[0, :] = 0
    return daily_returns

def test_run():
    """Function called by Test Run."""
    start_date = '2019-01-01'
    end_date = '2019-12-18'
    dates = pd.date_range(start_date, end_date)
    dfStockData = helper.get_data(['SPY','I', 'GOOG', 'IBM'], dates)
    daily_df = compute_daily_returns(dfStockData)
    # scatter plots plot how one stock moves in relation to another
    # over time a pattern can emerge and using linear regression a line can be
    # fitted to the data.
    # the slope of the line shows how reactive the stock is - BETA
    #  
    #if Beta == 1 then on average is the market or one stock goes up 1% the other will go up 1%
    #
    # ALPA when the line intersects 0
    #if ALPHA is positive then this stock on average performs better than the other or reference stock (e.g SPY)

    #correlation how tightly do the points fit the line 
    #higher the correlation means more accurate prediction on average
    daily_df.plot(kind='scatter', x='SPY', y='IBM')
    beta_IBM, alpha_IBM = np.polyfit(daily_df['SPY'], daily_df['IBM'],1)
    plt.plot(daily_df['SPY'], beta_IBM * daily_df['SPY'] + alpha_IBM, '-', color='r')
    plt.show()

    daily_df.plot(kind='scatter', x='SPY', y='GOOG')
    beta_Goog, alpha_Goog = np.polyfit(daily_df['SPY'], daily_df['GOOG'],1)
    plt.plot(daily_df['SPY'], beta_Goog * daily_df['SPY'] + alpha_Goog, '-', color='r')
    plt.show()

    #correlation
    print daily_df.corr(method='pearson')

if __name__ == "__main__":
    test_run()