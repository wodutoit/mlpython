import helper
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def compute_daily_returns(df):
    daily_returns = df.copy()
    daily_returns[1:] = (df[1:] / df[:-1].values) -1 #need to use .values otherwise the element wise arithmatic will not work
    daily_returns.ix[0, :] = 0
    return daily_returns

def test_run():
    """Function called by Test Run."""
    start_date = '2019-01-01'
    end_date = '2019-12-18'
    dates = pd.date_range(start_date, end_date)
    dfStockData = helper.get_data(['SPY','I', 'GOOG', 'IBM'], dates)
    allocations = [0.4,0.4,0.1,0.1]
    starting_amount = 1000000
    daily_df = compute_daily_returns(dfStockData)

    #norm the prices
    normalized = dfStockData / dfStockData.ix[0, :]
    allocated = normalized * allocations
    position_values = allocated * starting_amount
    portfolio_values = position_values.sum(axis=1)
    #show last row
    #print normalized[-1:]
    #print allocated[-1:]
    #print position_values[-1:]
    #print portfolio_values[-1:]
    closing_amount = portfolio_values[-1:]
    print 'P/L'
    print closing_amount - starting_amount


    # four key stats
    cumulative_returns = (portfolio_values[-1] / portfolio_values[0] ) -1
    # avg daily returns = portfolio_daily_returns.mean()
    # standard daily returns = portfolio_daily_returns.std()
    # sharpe ratio = 


if __name__ == "__main__":
    test_run()