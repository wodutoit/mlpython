import helper
import os
import pandas as pd
import matplotlib.pyplot as plt

def compute_daily_returns(df):
    daily_returns = df.copy()
    daily_returns[1:] = (df[1:] / df[:-1].values) -1 #need to use .values otherwise the element wise arithmatic will not work
    daily_returns.ix[0, :] = 0
    return daily_returns

def test_run():
    """Function called by Test Run."""
    start_date = '2019-01-01'
    end_date = '2019-12-18'
    dates = pd.date_range(start_date, end_date)
    dfStockData = helper.get_data(['I', 'GOOG', 'IBM'], dates)
    daily_df = compute_daily_returns(dfStockData)

    #helper.plot_data(daily_df, "daily returns")

    #Kurtosis curved bit of the histogram
    #if you have long tails large movement outside of the norm
    #positive kurtosis fat tails
    #negative kurtosis skinny tails
    daily_df['SPY'].hist(bins=30, label='SPY')
    daily_df['GOOG'].hist(bins=30, label='GOOG')
    daily_df['IBM'].hist(bins=30, label='IBM')
    iMean = daily_df['I'].mean()
    spyMean = daily_df['SPY'].mean()
    googMean = daily_df['GOOG'].mean()
    ibmMean = daily_df['IBM'].mean()
    
    plt.legend(loc='upper right')
    plt.show()


if __name__ == "__main__":
    test_run()