"""Compute mean volume"""

import pandas as pd
import matplotlib.pyplot as plt

def get_mean_volume(symbol):
    """Return the mean volume for stock indicated by symbol.
    
    Note: Data for a stock is stored in file: data/<symbol>.csv
    """
    df = pd.read_csv("data/{}.csv".format(symbol))  # read in data
    return df['Volume'].mean()
    
def plotSymbol(symbol):
    df = pd.read_csv("data/{}.csv".format(symbol))
    df[['Close','Adj Close']].plot()
    plt.show()

def test_run():
    """Function called by Test Run."""
    for symbol in ['AAPL', 'IBM']:
        print "Mean Volume"
        print symbol, get_mean_volume(symbol)
    #plot APPL
    plotSymbol('AAPL')

if __name__ == "__main__":
    test_run()