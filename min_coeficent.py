import helper
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as spo

def error(line, data): #error function how far are the points of data from our line
    """Compute the error between the line model and data
    Parameters
    ----------
    line: tuple/list/array (c0 and c1) where c0 is the slope and c1 is the y intercept
    data: array of 2D points (x,y)

    return error as a single real value (no negatives)"""
    err = np.sum((data[:, 1] - (line[0] * data[:, 0] + line[1])) ** 2)
    return err

def fit_line(data, error_func):
    l = np.float32([0,np.mean(data[:,1])])
    x_ends = np.float32([-5, 5])
    plt.plot(x_ends, l[0] * x_ends + l[1], 'm--', linewidth=2.0, label="Initial Guess")

    #call optimiser
    result = spo.minimize(error_func, l, args=(data,), method="SLSQP", options={"disp": True})
    return result.x

def test_run():
    #define line
    line_orig = np.float32([4,2])
    print "original line : c0 = {}, c1 = {}".format(line_orig[0], line_orig[1])
    Xorig = np.linspace(0,10,21)
    Yorig = line_orig[0] * Xorig + line_orig[1]
    plt.plot(Xorig, Yorig, 'b-', linewidth=2.0, label="Original Line")

    #noisy data
    noise_sigma = 3.0
    noise = np.random.normal(0, noise_sigma, Yorig.shape)
    data = np.asarray([Xorig, Yorig + noise]).T
    plt.plot(data[:,0], data[:,1],'go', label="Data Points")
    
    #try fit the line
    l_fit = fit_line(data, error)
    print "fitted line: C0 = {}, C1 = {}".format(l_fit[0], l_fit[1])
    plt.plot(data[:,0],l_fit[0]* data[:, 0] + l_fit[1], 'r--', linewidth=2.0, label="new line")
    
    plt.show()

if __name__ == "__main__":
    test_run()