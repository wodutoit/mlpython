import os
import pandas as pd
import matplotlib.pyplot as plt

def load_csv(symbol, datapath='data', use_cols=['Date', 'Adj Close']):
    return pd.read_csv("{}/{}.csv".format(datapath,symbol), index_col="Date", parse_dates=True, usecols=use_cols, na_values=['nan'])

def get_data(symbols, dates):
    dfStockData = pd.DataFrame(index=dates)
    dataPath = 'data'
    if 'SPY' not in symbols:
        symbols.insert(0, 'SPY')
    
    for symbol in symbols:
        dfSymbol = load_csv(symbol)
        dfSymbol = dfSymbol.rename(columns={'Adj Close' : symbol})
        if symbol == 'SPY':
            dfStockData = dfStockData.join(dfSymbol, how='inner')
        else:
            dfStockData = dfStockData.join(dfSymbol) #default left join
    #now we have all the data get rid of any gaps
    dfStockData.fillna(method='ffill', inplace=True)
    dfStockData.fillna(method='bfill', inplace=True)
    return dfStockData
    
def plot_data(df, title='Stock Prices', ylabel='Price', xlabel='Date'):
    '''plot data '''
    ax = df.plot(title=title)#, fontsize=20)
    ax.set_xlabel(ylabel)
    ax.set_ylabel(xlabel)
    plt.show()
