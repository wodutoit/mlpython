import helper
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as spo

def f(X):
    """given a scalar X, return the function output"""
    Y = (X - 1.5)**2 + 0.5
    print "X = {}, Y = {}".format(X, Y)
    return Y

def test_run():
    """Function called by Test Run."""
    Xguess = 2.0
    min_result = spo.minimize(f, Xguess, method="SLSQP", options={'disp':True})
    print "minima found at:"
    print "X = {}, Y = {}".format(min_result.x, min_result.fun)

    Xplot = np.linspace(0.5, 2.5, 21)
    Yplot = f(Xplot)
    plt.plot(Xplot, Yplot)
    plt.plot(min_result.x, min_result.fun, 'ro')
    plt.title("Minima of a function")
    plt.show()


if __name__ == "__main__":
    test_run()