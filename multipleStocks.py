import os
import pandas as pd
import matplotlib.pyplot as plt

def load_csv(symbol, datapath='data', use_cols=['Date', 'Adj Close']):
    return pd.read_csv("{}/{}.csv".format(datapath,symbol), index_col="Date", parse_dates=True, usecols=use_cols, na_values=['nan'])

def get_data(symbols, dates):
    dfStockData = pd.DataFrame(index=dates)
    dataPath = 'data'
    if 'SPY' not in symbols:
        symbols.insert(0, 'SPY')
    
    for symbol in symbols:
        dfSymbol = load_csv(symbol)
        dfSymbol = dfSymbol.rename(columns={'Adj Close' : symbol})
        if symbol == 'SPY':
            dfStockData = dfStockData.join(dfSymbol, how='inner')
        else:
            dfStockData = dfStockData.join(dfSymbol) #default left join
    return dfStockData
def plot_data(df, title='Stock Prices'):
    '''plot data '''
    ax = df.plot(title=title)#, fontsize=20)
    ax.set_xlabel("Date")
    ax.set_ylabel("Price")
    plt.show()

def test_run():
    """Function called by Test Run."""
    start_date = '2019-01-01'
    end_date = '2019-12-18'
    dates = pd.date_range(start_date, end_date)
    dfStockData = get_data(['GOOG', 'AAPL', 'IBM', 'I'], dates)
    #slicing
    print dfStockData.ix['2019-08-01':'2019-09-01', ['SPY', 'IBM'] ]
    #normalise data
    dfNorm = dfStockData / dfStockData.ix[0, :]
    plot_data(dfNorm, "Normalised Data")

if __name__ == "__main__":
    test_run()